essay:
	pdflatex essay.tex
	biber essay
	pdflatex essay.tex
	xdg-open essay.pdf

clean:
	git clean -dfx
